import controller from './forgotpassword.controller'
import template from './forgotpassword.template.html'

let forgotPasswordComponent = {
  bindings: {},
  template,
  controller,
  controllerAs: 'vm'
}

export default forgotPasswordComponent

import controller from './chatui.controller'
import template from './chatui.html'

let chatUiComponent = {
  bindings: {},
  template,
  controller,
  controllerAs: 'vm'
}

export default chatUiComponent

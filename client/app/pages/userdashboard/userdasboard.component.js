import template from './userdashboard.html'
import controller from './userdashboard.controller'

let userDashboardComponent = {
  bindings: {},
  template,
  controller,
  controllerAs: 'vm'
}

export default userDashboardComponent

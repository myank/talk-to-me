import controller from './home.controller'
import template from './home.template.html'

let homeComponent = {
  bindings: {},
  template,
  controller,
  controllerAs: 'vm'
}

export default homeComponent
